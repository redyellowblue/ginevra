import pygame
import sys

windowSurface = pygame.display.set_mode((1360, 768), pygame.FULLSCREEN | pygame.DOUBLEBUF)
image = pygame.image.load("ginevra.png").convert_alpha()
eyes = pygame.image.load("eyes.png")
eyesRect = pygame.Rect(785, 325, 55, 280)

def setup():
	global windowSurface
	global image
	global eyes
	
	pygame.init()
	windowSurface.blit(eyes, (0, 0))
	windowSurface.blit(image, (0, 0))
	pygame.display.flip()
	
	for rep in range(0, 3):
		for i in range(-25, 25):
			showAt(i)
		for i in range(25, -25, -1):
			showAt(i)
	
	pygame.quit()
	sys.exit()

def loop():
	# Do nothing
	print("Hello World!")
	
def showAt(i):
	global windowSurface
	global image
	global eyes
	global eyesRect
	
	windowSurface.blit(eyes, (0, i))
	windowSurface.blit(image, (0, 0))
	#pygame.draw.rect(windowSurface, pygame.Color('red'), eyesRect, 3)
	pygame.display.update(eyesRect)

setup()
while True:
	loop()
