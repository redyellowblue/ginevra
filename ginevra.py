from imutils.video import VideoStream
import argparse
import datetime
import imutils
import pygame
import os
import sys
import time
import datetime
import cv2
import math
import traceback

mode = "ginevra" # values: debug, ginevra
background = None
windowSurface = None
image = None
eyes = None
eyesRect = pygame.Rect(785, 325, 55, 280)
eyeShift = 0

lastTempCheck = datetime.datetime.now()
uncomfortablyHot = 72 # CPU temp at which the program pauses to cool off
tooHot = 79 # CPU temp at which the program shuts down

vs = None
minArea = 500
	
def setup():
	global mode
	global vs
	
	# check for any command line arguments
	if len(sys.argv) > 1 and sys.argv[1] != None:
		mode = sys.argv[1]
	
	vs = VideoStream(src=0).start()
	time.sleep(2.0) # give the videostream some time to start up
	
	if mode == "ginevra":
		initPygame()
		
		moveTo(-30)
		moveTo(30)
		moveTo(0)
		
def loop():
	global mode
	
	checkForEvents()
	
	# get the current frame
	frame = vs.read()
	if frame is None:
		stop()
	
	contours = findContours(frame)
	tallestContour = findTallestContour(contours)
	if tallestContour is not None:
		centerOfTallestContour = getCenterOfContour(tallestContour)
		eyeDestination = calculateEyeDestination(centerOfTallestContour)
	
		if mode == "ginevra":
			# move the eyes
			moveTo(eyeDestination)
	
	if mode != "ginevra":
		# show some images and info to help with debugging
		showDiagnostics(frame, tallestContour)
	
	# try not to overheat the poor little struggling Raspberry Pi
	dontLetPiGetTooHot()
	time.sleep(.1)
	
def initPygame():
	global windowSurface
	global image
	global eyes
	
	windowSurface = pygame.display.set_mode((1360, 768), pygame.FULLSCREEN | pygame.DOUBLEBUF)
	image = pygame.image.load("/home/pi/Documents/Ginevra/ginevra.png").convert_alpha()
	eyes = pygame.image.load("/home/pi/Documents/Ginevra/eyes.png")
	
	pygame.init()
	pygame.mouse.set_visible(False)
	windowSurface.blit(eyes, (0, 0))
	windowSurface.blit(image, (0, 0))
	pygame.display.flip()
	
def moveTo(eyeDestination):
	global eyeShift
	global windowSurface
	global eyes
	global image
	global eyesRect
		
	while eyeShift != eyeDestination:
		if eyeShift - eyeDestination > 5:
			eyeShift -= 2
		elif eyeShift - eyeDestination < -5:
			eyeShift += 2
		elif eyeDestination < eyeShift:
			eyeShift -= 1
		elif eyeDestination > eyeShift:
			eyeShift += 1

		# update the screen
		windowSurface.blit(eyes, (0, eyeShift))
		windowSurface.blit(image, (0, 0))
		pygame.display.update(eyesRect)
	
def findContours(frame):
	global background
	
	frame = imutils.resize(frame, width=500)
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (21, 21), 0)

	if background is None:
		background = gray

	# compute the difference between frames
	# based on https://www.pyimagesearch.com/2015/05/25/basic-motion-detection-and-tracking-with-python-and-opencv/
	frameDelta = cv2.absdiff(background, gray)
	thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
	thresh = cv2.dilate(thresh, None, iterations=2)

	contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
		cv2.CHAIN_APPROX_SIMPLE)
	contours = imutils.grab_contours(contours)
	return contours

def findTallestContour(contours):
	global minArea
	
	tallestContourHeight = 0
	tallestContour = None
	for c in contours:
		# if the contour is too small, ignore it
		if cv2.contourArea(c) < minArea:
			continue

		# compute the bounding box for the contour
		(x, y, w, h) = cv2.boundingRect(c)
		
		if h > tallestContourHeight:
			tallestContourHeight = h
			tallestContour = c
	
	return tallestContour
	
def getCenterOfContour(c):
	(x, y, w, h) = cv2.boundingRect(c)
	return x + (w/2)
	
def calculateEyeDestination(centerOfTallestContour):
	#eyeDestination = int(round(-1 *((centerOfTallestContour / 10) - 25)))
	eyeDestination = -1 * int(round(50/(1+math.pow(math.e, -.01*(centerOfTallestContour-250)))-25))
	
	if centerOfTallestContour < 0:
		eyeDestination = 0
		
	return eyeDestination
	
def showDiagnostics(frame, contour):
	#cv2.putText(frame, "Test", (10, 20),
	#	cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	
	# show the contour on the image
	(x, y, w, h) = cv2.boundingRect(contour)
	cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
	
	# rotate due to the sideways screen
	frame = imutils.rotate(frame, 270)
	
	cv2.imshow("frame", frame)

def switchToDebugMode():
	global mode
	mode = "debug"
	pygame.quit()

def switchToGinevraMode():
	global mode
	mode = "ginevra"
	cv2.destroyAllWindows()
	initPygame()

def checkForEvents():
	global mode
	global background
	global windowSurface
	
	if mode == "ginevra":
		for event in pygame.event.get():
			if event.type==pygame.QUIT:
				stop()
			elif event.type == pygame.KEYDOWN:
				if event.key == pygame.K_ESCAPE or event.key == pygame.K_q:
					stop()
				elif event.key == pygame.K_r:
					background = None
				elif event.key == pygame.K_p:
					pygame.image.save(windowSurface, "screenshot.jpg")
				elif event.key == pygame.K_d or event.key == pygame.K_g:
					switchToDebugMode()
					background = None
	else:
		key = cv2.waitKey(1) & 0xFF
		if key == ord("q"):
			stop()
		elif key == ord("r"):
			background = None
		elif key == ord("g") or key == ord("d"):
			switchToGinevraMode()
			background = None

def getPiTemp():
	# run a command to get the temperature
	# the value will be something like "temp=51.5'C"
	# this takes about .02 seconds, so don't run it too often
	tempStr = os.popen("vcgencmd measure_temp").readline()
	tempStr = tempStr.replace("temp=", "").replace("'C", "")
	temp = float(tempStr)
	return temp

def dontLetPiGetTooHot():
	global lastTempCheck
	
	# check the temperature occasionally
	now = datetime.datetime.now()
	timeSinceLastTempCheck = now - lastTempCheck
	if timeSinceLastTempCheck.total_seconds() > 10:
		lastTempCheck = now
		
		temp = getPiTemp()
		
		if temp > tooHot:
			# Something has gone wrong. Stop the program.
			time.sleep(2)
			for i in range(0, 10):
				print(".")
			print(str(temp))
			print("Overheating!!!")
			print("Wait for the number next to the time to get below 60 degrees.")
			print("Then press the up arrow in the terminal.")
			print("Then press Enter.")
			for i in range(0, 10):
				print(".")
			stop()
		elif temp > uncomfortablyHot:
			# the pi is hot and needs some time to cool down
			time.sleep(1)
			print("Hot! " + str(temp))
			
			# keep checking the temperature and sleeping until it cools down
			while getPiTemp() > uncomfortablyHot:
				time.sleep(1)
				
			cooldownTime = datetime.datetime.now() - lastTempCheck
			print("Cooldown time: " + str(cooldownTime.total_seconds()))

def stop():
	global vs
	global mode
	
	vs.stop()
	
	if mode == "ginevra":
		pygame.quit()
	else:
		cv2.destroyAllWindows()
	
	sys.exit()

try:
	setup()
	while True:
		loop()
except Exception as e:
	traceback.print_exc()
	raise()
finally:
	stop()
